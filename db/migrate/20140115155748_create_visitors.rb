class CreateVisitors < ActiveRecord::Migration
  def up
  end

  def change
    create_table :visits do |t|
      t.integer :visitor_id, null: false
      t.string :shortened_url, null: false

      t.timestamps
    end

    add_index(:visits, :shortened_url)

  end

  def down
  end
end
