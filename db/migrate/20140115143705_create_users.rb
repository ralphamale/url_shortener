class CreateUsers < ActiveRecord::Migration
  def up
  end

  def change
    create_table :users do |t|
      t.string :email, unique: true, null: false

      t.timestamps
    end

    create_table :shortened_urls do |t|
      t.string :short_url, unique: true
      t.string :long_url, null: false
      t.integer :submitter_id, null: false

      t.timestamps
    end

    add_index(:shortened_urls, :submitter_id)
    add_index(:shortened_urls, :short_url)

  end

  def down
  end
end
