class FiveminsValidator < ActiveModel::EachValidator

  def validate_each(record, attribute_name, value)

    if ShortenedUrl.where(:submitter_id => value, :created_at => 1.minutes.ago..DateTime.current).count > 5
      message = options[:message] || "CANT SUBMIT TOO MUCH IN A MINUTE"
      record.errors[attribute_name] << message
    end
  end

end
