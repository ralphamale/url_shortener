class Visit < ActiveRecord::Base

  attr_accessible :visitor_id, :shortened_url
  validates :visitor_id, :shortened_url, presence: true

  belongs_to(
    :visitor,
    :class_name => "User",
    :foreign_key => :visitor_id,
    :primary_key => :id
    )

    belongs_to(
    :shortened_urlq,
    :class_name => "ShortenedUrl",
    :foreign_key => :shortened_url,
    :primary_key => :short_url
    )

  def self.record_visit!(user, shortened_urlx)
    Visit.new({:visitor_id => user.id, :shortened_url => shortened_urlx}).save!
  end

end