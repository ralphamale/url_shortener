class ShortenedUrl < ActiveRecord::Base
  attr_accessible :short_url, :long_url, :submitter_id, :created_at
  validates :short_url, :long_url, :submitter_id, :presence => true
  validates :short_url, :uniqueness => true
  validates :submitter_id, :fivemins => true

  belongs_to(
  :submitter,
  :class_name => "User",
  :foreign_key => :submitter_id,
  :primary_key => :id
  )

  has_many(
  :visited_urls,
  :class_name => "Visit",
  :foreign_key => :shortened_url,
  :primary_key => :short_url
  )

  has_many :visitors, :through => :visited_urls, :source => :visitor, :uniq => true

  def self.random_code
    loop do
      short_code = SecureRandom.urlsafe_base64(16)
      return short_code if ShortenedUrl.where("short_url = ?",
      short_code).empty?
    end
  end

  def self.create_for_user_and_long_url!(user, long_url)
    short_url = ShortenedUrl.random_code
    ShortenedUrl.new({:short_url => short_url, :long_url => long_url, :submitter_id => user.id}).save!
    short_url
  end

  def num_clicks
    Visit.where(:shortened_url => self.short_url).count
  end

  def num_uniques
    Visit.where(:shortened_url => self.short_url).count(:visitor_id, :distinct => true)
  end

  def num_recent_uniques
    Visit.where(:shortened_url => self.short_url, :created_at => 10.minutes.ago..DateTime.current).count(:visitor_id, :distinct => true)
  end

end